# React Beer App


## Opis

Nasza aplikacja jest możliwością dla koneserów **piwa** 🍻, aby mogli oni dzielić się swoimi odczuciami i wszelkimi wrażeniami z konsumpcji z innymi smakoszami. Jest ona również pomocną dłonią dla nowicjuszy którzy dopiero zaczynają swoją przygodę z degustacją.
---
Inaczej mówiąc: Dosyć prosta aplikacja w *react* jako projekt zaliczeniowy, w założeniu jest znacznie uproszczoną wersją https://ocen-piwo.pl/

## Działanie strony: 
1. Na ekranie głównym kafelki z wraz z jego oceną, ułożone w kolejności ich dodania
2. W headerze nazwa strony, jak i guzik umożliwiający dodania nowej recenzji piwa.

*Po wciśnięciu guzika, formularz do wypełnienia w którym podajemy:* 
- nazwę
- woltaż, słód, słodkość, słodowość
- ocena od 1 do 10
- pisemna recenzja smaku

3. Możliwość późniejszej edycji jak i usuwania recenzji

## Uruchamianie projektu
1. Klonowanie repozytorium
`git clone https://gitlab.com/sebastian.lapinski/react-beer-app.git`

2. Instalacja wszystkich potrzebnych node_modules
`npm install`

3. Samo uruchomienie aplikacji
`npm start`


#### Autorzy
- Sebastian Łapiński
- Damian Moniuszko
- Mateusz Kraśnicki
