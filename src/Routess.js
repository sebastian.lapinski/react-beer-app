import {BrowserRouter as Router, Routes, Route, Navigate} from "react-router-dom";
import React from 'react'
import {Home, Addbeer} from './pages';

const Routess = () => {
  return (
    <Router>
      <Routes>
        <Route path='/' element={<Navigate to="/home"/>}/>
        <Route path="/home" element={<Home/>}/>
        <Route path="/addbeer" element={<Addbeer/>}/>
      </Routes>
    </Router>
  )
}

export default Routess