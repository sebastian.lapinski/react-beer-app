import React from 'react'
import './footer.css'
const Footer = () => {
  return (
    <div className="footer">
        <p>Autorzy: Sebastian Łapiński, Mateusz Kraśnicki,</p><span>Damian Moniuszko</span>
    </div>
  )
}

export default Footer