import React from 'react'
import './header.css'
import plusicon from '../../assets/plusicon.png'
import { Link } from 'react-router-dom'

const Header = () => {
  return (
    <div className="header-container">
      <Link className="link" to={"/home"}><p className="logo">react-beer-app</p></Link>
      <Link className="link" to={"/addbeer"}><button className="addpiwo"><img src={plusicon}/>DODAJ PIWO</button></Link>
    </div>
  )
}

export default Header