import React from 'react'
import './addbeerform.css'
import * as fs from "fs";

const savedata = () => {
    let nazwa = document.getElementById("nazwa").value
    let woltaz = document.getElementById("woltaz").value
    let goryczka = document.getElementById("goryczka").value
    let slodowosc = document.getElementById("slodowosc").value
    let slodkosc = document.getElementById("slodkosc").value
    let ogolna = document.getElementById("ogolna").value
    let opis = document.getElementById("opis").value
          let data = {
        name : nazwa,
        woltaz : woltaz,
        goryczka : goryczka,
        slodowosc : slodowosc,
        slodkosc : slodkosc,
        ogolna : ogolna,
        opis : opis
      }
      let beers = require('../../beerdata.json')
      beers.push(data)
      fs.writeFile('../../beerdata.json', JSON.stringify(beers), err =>{
        if(err) throw err
      })
    // if (nazwa == "" || woltaz==null || goryczka==null || slodowosc==null || slodkosc==null || ogolna==null || opis==null){
    // // document.getElementById("errormessage").innerHTML = "Należy wypełnić wszystkie pola";
    // } else{
    //   let data = {
    //     name : nazwa,
    //     woltaz : woltaz,
    //     goryczka : goryczka,
    //     slodowosc : slodowosc,
    //     slodkosc : slodkosc,
    //     ogolna : ogolna,
    //     opis : opis
    //   }
    //   let beers = require('../../beerdata.json')
    //   beers.push(data)
    //   fs.writeFile('../../beerdata.json', JSON.stringify(beers), err =>{
    //     if(err) throw err
    //   })
    // }
}

const Addbeerform = () => {
  return (

    <div className="container">
      <div className="formcontainer">
          <p>Nazwa</p><input type="text" id="nazwa"/>
          <p>Woltaż</p><input type="number" id="woltaz" placeholder='%'/>
          <p>Goryczka</p><input type="number" id="goryczka" placeholder='1-10'/>
          <p>Słodowość</p><input type="number" id="slodowosc" placeholder='1-10'/>
          <p>Słodkość</p><input type="number" id="slodkosc" placeholder='1-10'/>
          <p>Ocena ogólna</p><input type="number" id="ogolna" placeholder='1-10'/>
          <p>Opis</p><input type="text" id="opis" />
          <button className="dodaj" onClick={savedata}>Dodaj</button>
          <p className="errormessage"></p>
      </div>
    </div>

  )
}

export default Addbeerform