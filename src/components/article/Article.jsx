import React from 'react'
import './article.css'
import beerplaceholder from '../../assets/beerplaceholder.jpg'
const Article = (props) => {
  return (
    <div className="article">
      <img src={beerplaceholder} alt="piwo"/>
      <div className="info">
        <div className="namedesc">
          <p className="beername">{props.name}</p>
          <p className="description">Opis: {props.opis}</p>
        </div>
        <div className="stats">
          <p>Woltaż: {props.woltaz}% <br/>
          Goryczka: {props.goryczka}/10 <br/>
          Słodowość: {props.slodowosc}/10 <br/>
          Słodkość: {props.slodkosc}/10</p> 
          <p className="rating">Ocena ogólna: {props.ogolna}/10</p>
        </div>
      </div>
    </div>
  )
}

export default Article