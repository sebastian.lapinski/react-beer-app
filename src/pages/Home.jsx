import React from 'react'
import data from '../beerdata.json'
import '../App.css'
import { Header, Article, Footer } from '../components'

const Home = () => {
  return(
    <div className="app">
      <Header/>
      {
        data.map((data, i) => (
          <Article key={i} name={data.name} woltaz={data.woltaz} goryczka={data.goryczka} slodowosc={data.slodowosc} slodkosc={data.slodkosc} ogolna={data.ogolna} opis={data.opis}/>
        ))
      }
      <Footer/>
    </div>
  )
}

export default Home