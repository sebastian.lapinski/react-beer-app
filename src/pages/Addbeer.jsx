import React from 'react'
import data from '../beerdata.json'
import '../App.css'
import { Header, Article, Footer, Addbeerform } from '../components'


const Addbeer = () => {
  return (
    <div className="app">
      <Header/>
      <Addbeerform/>
      <Footer/>
    </div>
  )
}

export default Addbeer